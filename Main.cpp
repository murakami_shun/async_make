﻿
# include <Siv3D.hpp>
# include "async_make.hpp"
#include "thread_pool.hpp"

// 生成に時間がかかるリソース
class HeavyClass {
public:
	HeavyClass(StringView str) {
		std::this_thread::sleep_for(3s);
		Console << str;
	}
	void test() {}
};

void Main() {
	Console.open();
	// 非同期で生成
	auto resource = async_make<HeavyClass>(U"resource");
	
	async_sequential_maker sequential_maker;
	auto res1 = sequential_maker.make<HeavyClass>(U"res1");
	auto res2 = sequential_maker.make<HeavyClass>(U"res2");
	auto res3 = sequential_maker.make<Texture>(Emoji(U"🐣"));

	Scene::SetBackground(ColorF(0.8, 0.9, 1.0));

	const Font font(60);

	const Texture cat(Emoji(U"🐈"));


	while (System::Update()) {
		if (res3) {
			font(U"全ロード完了").drawAt(Scene::Center(), Palette::Black);
			res3->resized(100 + Periodic::Sine0_1(1s) * 20).drawAt(Scene::Center() + Point{ 0, 100 });
		}
		else if (resource) {
			font(U"ロード完了").drawAt(Scene::Center(), Palette::Black);
			cat.resized(100 + Periodic::Sine0_1(1s) * 20).drawAt(Scene::Center() + Point{ 0, 100 });
		}
		else {
			font(U"ロード中").drawAt(Scene::Center(), Palette::Black);
			cat.rotated(Periodic::Sine0_1(1s)-0.5).drawAt(Scene::Center() + Point{ 0, 100 });
		}
	}
}
