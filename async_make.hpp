#pragma once

#include <thread>
#include <memory>
#include <future>
#include <atomic>

template<typename Obj>
class future_object {
private:
	mutable std::unique_ptr<Obj> obj;
	mutable std::future<std::unique_ptr<Obj>> future;
	std::shared_ptr<std::atomic_bool> done;
public:
	future_object() :
		done(std::make_shared<std::atomic_bool>(false))
	{
	}

	future_object(std::future<std::unique_ptr<Obj>>&& fut, std::shared_ptr<std::atomic_bool>&& flag) :
		done(std::move(flag)),
		future(std::move(fut))
	{
	}

	future_object(future_object&& src) noexcept {
		operator=(std::forward<future_object>(src));
	}

	future_object& operator=(future_object&& src) noexcept {
		future = std::move(src.future);
		obj = std::move(src.obj);
		done = src.done;
		return *this;
	}

	future_object(const future_object& src) = delete;
	future_object& operator=(const future_object& src) = delete;
	
	void wait() const {
		if (!future.valid()) return;
		future.wait();
		obj = future.get();
	}
	
	Obj* get() const noexcept {
		return obj.get();
	}
	
	Obj* operator ->() const noexcept {
		return get();
	}

	explicit operator bool() const noexcept {
		if (!obj && *done) wait();
		return *done;
	}

	operator std::unique_ptr<Obj>() && {
		if (!obj) wait();
		return std::move(obj);
	}
};

template<class Obj, typename... Args>
auto async_make(Args&&... args) {
	auto done = std::make_shared<std::atomic_bool>(false);
	std::promise<std::unique_ptr<Obj>> prm;
	auto future = prm.get_future();
	std::thread([args...](std::promise<std::unique_ptr<Obj>> prm, std::shared_ptr<std::atomic_bool> done){
		prm.set_value(std::make_unique<Obj>(args...));
		*done = true;
	}, std::move(prm), done).detach();
	return future_object<Obj>(std::move(future), std::move(done));
}

#include "thread_pool/thread_pool.hpp"

class async_sequential_maker {
private:
	thread_pool pool;
public:
	async_sequential_maker() {}

	template<class Obj, typename... Args>
	auto make(Args&&... args) {
		auto done = std::make_shared<std::atomic_bool>(false);
		auto prm = std::make_shared<std::promise<std::unique_ptr<Obj>>>();
		auto future = prm->get_future();
		pool.post([...args=std::forward<Args>(args)](std::shared_ptr<std::promise<std::unique_ptr<Obj>>> prom, std::shared_ptr<std::atomic_bool> done) {
			prom->set_value(std::make_unique<Obj>(args...));
			*done = true;
		}, prm, done);
		return future_object<Obj>(std::move(future), std::move(done));
	}
};